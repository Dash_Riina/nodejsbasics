// const http = require('http'); // require - это подключение модуля
//
// const hostname = '127.0.0.1';
// const port = 3000;
//
// const server = http.createServer((req, res) => { // создаем сервер через http
//     res.statusCode = 200;
// res.setHeader('Content-Type', 'text/plain');
// res.write(req.url);
// res.end('Hello World\n');
// });
//
// server.listen(port, hostname, () => {
//     console.log(`Server running at http://${hostname}:${port}/`);
// });
//
//
//
// // читаем файл
// var http = require('http');
// var fs = require('fs');
// http.createServer(function (req, res) {
//     fs.readFile('index.html', function (err, data) {
//         res.writeHead(200, {'Content-type': 'text/html'});
//         res.write(data);
//         res.end();
//     });
// }).listen(3000);
//
// // создаем файл
// fs.open('opened.txt', 'w', function(err){
//     console.log('create with open done');
// });
//
// // пишем в файл
// fs.writeFile('writable.txt', '456', function (err) {
//     console.log('writing down');
// });

const http = require('http'),
    fs = require('fs');

// open не работает :(
// let index = fs.open('index.html', 'r', function (err) {
//     console.log('done');
//     console.log(data);
// });

let index = fs.readFile('index.html', function (err, data) {
    console.log(data.toString());
});

